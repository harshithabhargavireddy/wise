import tkinter as tk
from tkinter import ttk
import sqlite3
from pyshorteners import Shortener


url_shortener = Shortener()


def create_table():
    conn = sqlite3.connect('url_shortener.db')
    c = conn.cursor()
    c.execute('''CREATE TABLE IF NOT EXISTS urls
                 (id INTEGER PRIMARY KEY AUTOINCREMENT, original_url TEXT, short_url TEXT)''')
    conn.commit()
    conn.close()

def insert_url_pair(original_url, short_url):
    conn = sqlite3.connect('url_shortener.db')
    c = conn.cursor()
    c.execute('''INSERT INTO urls (original_url, short_url) VALUES (?, ?)''', (original_url, short_url))
    conn.commit()
    conn.close()

def shorten_url():
    long_url = url_entry.get()

    short_url = url_shortener.tinyurl.short(long_url)

    insert_url_pair(long_url, short_url)

    original_label.config(text=f"Original URL: {long_url}", fg="blue", font=("Helvetica", 12))  # Change text color to blue and increase font size
    shorten_label.config(text=f"Shortened URL: {short_url}", fg="green", font=("Helvetica", 12))  # Change text color to green and increase font size

    display_database_data()

def display_database_data():
    for row in treeview.get_children():
        treeview.delete(row)

    conn = sqlite3.connect('url_shortener.db')
    c = conn.cursor()
    c.execute('''SELECT * FROM urls''')
    data = c.fetchall()
    conn.close()

    for row in data:
        treeview.insert('', 'end', values=row)

root = tk.Tk()
root.geometry("800x500")
root.configure(background='lavender')  # Set background color

url_label = tk.Label(root, text="Enter URL:", bg="lavender", font=("Helvetica", 12))  # Set background color and increase font size
url_label.pack()
url_entry = tk.Entry(root, width=70, font=("Helvetica", 12))  # Increase font size
url_entry.pack()
shorten_button = tk.Button(root, text="Shorten URL", command=shorten_url, bg="orange", fg="white", font=("Helvetica", 12))  # Set button color and increase font size
shorten_button.pack()

original_label = tk.Label(root, text="", wraplength=750, anchor="w", bg="lavender", font=("Helvetica", 12))  # Set background color and increase font size
original_label.pack()
shorten_label = tk.Label(root, text="", wraplength=750, anchor="w", bg="lavender", font=("Helvetica", 12))  # Set background color and increase font size
shorten_label.pack()

treeview = ttk.Treeview(root, columns=('ID', 'Original URL', 'Short URL'), show='headings', height=15)
treeview.heading('ID', text='ID')
treeview.heading('Original URL', text='Original URL')
treeview.heading('Short URL', text='Short URL')
treeview.column('ID', width=50, anchor='center')
treeview.column('Original URL', width=400, anchor='center')  # Adjusted width to accommodate longer URLs
treeview.column('Short URL', width=400, anchor='center')     # Adjusted width to accommodate longer URLs
treeview.pack()

create_table()

display_database_data()

root.mainloop()