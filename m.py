import tkinter as tk
from pyshorteners import Shortener

# Initialize the URL shortener
url_shortener = Shortener()

def shorten_url():
    # Get the long URL from the entry field
    long_url = url_entry.get()

    # Shorten the URL
    short_url = url_shortener.tinyurl.short(long_url)

    # Display the short URL in the result label
    result_label.config(text=f"Short URL: {short_url}")

# Create the main window
root = tk.Tk()
root.geometry("500x500")
# Create a label, entry field, and button for the user to input their long URL
url_label = tk.Label(root, text="Long URL:")
url_label.pack()
url_entry = tk.Entry(root, width=50)
url_entry.pack()
shorten_button = tk.Button(root, text="Shorten URL", command=shorten_url)
shorten_button.pack()

# Create a label to display the short URL
result_label = tk.Label(root, text="")
result_label.pack()

# Start the Tkinter main loop
root.mainloop()