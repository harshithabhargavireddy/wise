import tkinter as tk
from tkinter import ttk
import sqlite3
from pyshorteners import Shortener

# Initialize the URL shortener
url_shortener = Shortener()

# Function to create the database table if it doesn't exist
def create_table():
    conn = sqlite3.connect('url_shortener.db')
    c = conn.cursor()
    c.execute('''CREATE TABLE IF NOT EXISTS urls
                 (id INTEGER PRIMARY KEY AUTOINCREMENT, original_url TEXT, short_url TEXT)''')
    conn.commit()
    conn.close()

# Function to insert the URL pair into the database
def insert_url_pair(original_url, short_url):
    conn = sqlite3.connect('url_shortener.db')
    c = conn.cursor()
    c.execute('''INSERT INTO urls (original_url, short_url) VALUES (?, ?)''', (original_url, short_url))
    conn.commit()
    conn.close()

# Function to shorten the URL and store it in the database
def shorten_url():
    # Get the long URL from the entry field
    long_url = url_entry.get()

    # Shorten the URL
    short_url = url_shortener.tinyurl.short(long_url)

    # Insert URL pair into the database
    insert_url_pair(long_url, short_url)

    # Display both the original URL and the shortened URL on the window
    original_label.config(text=f"Original URL: {long_url}", fg="blue")
    shorten_label.config(text=f"Shortened URL: {short_url}", fg="green")

    # Refresh the treeview to display updated data
    display_database_data()

# Function to display database data in the Treeview
def display_database_data():
    # Clear existing data in the treeview
    for row in treeview.get_children():
        treeview.delete(row)

    # Fetch data from the database
    conn = sqlite3.connect('url_shortener.db')
    c = conn.cursor()
    c.execute('''SELECT * FROM urls''')
    data = c.fetchall()
    conn.close()

    # Insert data into the treeview
    for row in data:
        treeview.insert('', 'end', values=row)

# Create the main window
root = tk.Tk()
root.geometry("800x500")

# Create a label, entry field, and button for the user to input their long URL
url_label = tk.Label(root, text="Enter URL:", fg="red")
url_label.pack()
url_entry = tk.Entry(root, width=70)
url_entry.pack()
shorten_button = tk.Button(root, text="Shorten URL", command=shorten_url, fg="purple")
shorten_button.pack()

# Create labels to display the original URL and the shortened URL
original_label = tk.Label(root, text="", wraplength=750, anchor="w")
original_label.pack()
shorten_label = tk.Label(root, text="", wraplength=750, anchor="w")
shorten_label.pack()

# Create a Treeview to display database data
treeview = ttk.Treeview(root, columns=('ID', 'Original URL', 'Short URL'), show='headings', height=15)
treeview.heading('ID', text='ID')
treeview.heading('Original URL', text='Original URL')
treeview.heading('Short URL', text='Short URL')
treeview.column('ID', width=50, anchor='center')
treeview.column('Original URL', width=400, anchor='center')
treeview.column('Short URL', width=400, anchor='center')
treeview.pack()

# Create the database table
create_table()

# Display database data initially
display_database_data()

# Start the Tkinter main loop
root.mainloop()