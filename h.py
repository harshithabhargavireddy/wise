import tkinter as tk
from tkinter import messagebox
from PIL import Image, ImageTk
from pyshorteners import Shortener

class URLShortenerApp:
    def __init__(self, root):
        self.root = root
        self.root.geometry("740x560+400+150")
        self.root.resizable(False, False)  
        self.root.title("URL Shortener")
        self.root.iconbitmap(r"C:/Users/harsh/OneDrive/Desktop/wise/icon1.ico")
        self.background_image = Image.open("C:/Users/harsh/OneDrive/Desktop/wise/bg.png")
        self.background_photo = ImageTk.PhotoImage(self.background_image)
        
        self.background_canvas = tk.Canvas(self.root, width=740, height=560)
        self.background_canvas.pack(fill="both", expand=True)
        self.background_canvas.create_image(0, 0, anchor="nw", image=self.background_photo)

        self.url_shortener = Shortener()

        self.url_frame = tk.Frame(self.background_canvas, bg="white")
        self.url_frame.place(relx=0.5, rely=0.3, anchor="center")

        self.url_label = tk.Label(self.url_frame, text="Enter URL:", bg="white", font=("Helvetica", 12))
        self.url_label.grid(row=0, column=0, padx=(0, 10))

        self.url_entry = tk.Entry(self.url_frame, width=50, font=("Helvetica", 12))
        self.url_entry.grid(row=0, column=1, padx=(0, 10))

        self.shorten_button = tk.Button(self.background_canvas, text="Shorten URL", command=self.shorten_url, bg="orange", fg="white", font=("Helvetica", 12))
        self.shorten_button.place(relx=0.5, rely=0.4, anchor="center")

        self.result_frame = tk.Frame(self.background_canvas, bg="white")
        self.result_frame.place(relx=0.5, rely=0.5, anchor="center")

        self.original_label = tk.Label(self.result_frame, text="", wraplength=740, anchor="w", bg="white", font=("Helvetica", 12))
        self.original_label.pack()

        self.shorten_label = tk.Label(self.result_frame, text="", wraplength=740, anchor="w", bg="white", font=("Helvetica", 12))
        self.shorten_label.pack()

        self.copy_button = tk.Button(self.background_canvas, text="Copy Shortened URL", bg="blue", fg="white", font=("Helvetica", 12), command=self.copy_to_clipboard)
        self.copy_button.place(relx=0.5, rely=0.6, anchor="center")

    def shorten_url(self):
        long_url = self.url_entry.get()
        if long_url.strip() == "":
            messagebox.showerror("Error", "Please enter a URL.")
            return

        try:
            short_url = self.url_shortener.tinyurl.short(long_url)
            self.original_label.config(text=f"Original URL: {long_url}", fg="blue")
            self.shorten_label.config(text=f"Shortened URL: {short_url}", fg="green")
            self.copy_button.config(command=lambda: self.copy_to_clipboard(short_url))
            self.url_entry.delete(0, tk.END) 
        except Exception as e:
            messagebox.showerror("Error", f"An error occurred: {str(e)}")

    def copy_to_clipboard(self, short_url):
        self.root.clipboard_clear()
        self.root.clipboard_append(short_url)
        messagebox.showinfo("Success", "Shortened URL copied to clipboard!")

if __name__ == "__main__":
    root = tk.Tk()
    app = URLShortenerApp(root)
    root.mainloop()